.data
values:        .word      2, 7, 7, 10, 3, 6
sec.min:    .asciiz     "sec.min: "
nl:     .asciiz     "\n"

    .text
    .globl  main

main:
    la      $s0,values                
    lw      $s2,0($s0)
    addi    $s1,$s0, 24
    add      $s3 $zero $zero
           

L1:
    beq     $s0,$s1,L2              

    lw      $t0,0($s0)              
    addi    $s0,$s0,4               


    slt     $t2,$t0,$s2            
    beq     $t2,$zero,L1
    move    $s3,$s2
    move    $s2,$t0

    j       L1

L2:
    li      $v0,4
    la      $a0,sec.min
    syscall

    li      $v0,1
    move    $a0,$s3                
    syscall
    
    li      $v0,4
    la      $a0,nl
    syscall

    li      $v0,10
    syscall

L3:
    move    $s2,$t0                 
    j       L1
   
    
    
    
    
    